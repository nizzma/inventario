#!/usr/bin/env python3

inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    for codigo, valores in inventario.items():
        print(f"{codigo}: {valores['nombre']}, precio: {valores['precio']}, cantidad: {valores['cantidad']}")
    print()


def consultar(codigo: str):
    if codigo in inventario:
        valores = inventario[codigo]
        print(f"{codigo}: {valores['nombre']}, precio: {valores['precio']}, cantidad: {valores['cantidad']}\n")
    else:
        print("Producto no encontrado.\n")


def agotados():
    agotados = [codigo for codigo, valores in inventario.items() if valores['cantidad'] == 0]
    if agotados:
        for codigo in agotados:
            print(f"El articulo {codigo} se ha agotado")
        print()
    else:
        print("No hay productos agotados.\n")


def pide_articulo() -> (str, str, float, int):
    codigo = input("Código de artículo: ")
    nombre = input("Nombre: ")
    precio = float(input("Precio: "))
    cantidad = int(input("Cantidad: "))
    return codigo, nombre, precio, cantidad


def menu() -> int:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
            print("Producto insertado correctamente.\n")
        elif opcion == '2':
            listar()
        elif opcion == '3':
            codigo = input("Código del artículo a consultar: ")
            consultar(codigo)
        elif opcion == '4':
            agotados()
        else:
            print("Opción incorrecta. Inténtalo de nuevo.\n")


if __name__ == '__main__':
    main()
